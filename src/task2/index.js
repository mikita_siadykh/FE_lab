function reverseString(str) {
}

function startWith(str, substr) {
}

function endWith(str, substr) {
}

function isCamelCase(str) {
}

function isSnakeCase(str) {
}

function isFalsy(x) {
}

function isNaN(x) {
}

function isFinite(x) {
}

if (window.globals && !window.globals.isTest) {
  reverseString();
  startWith();
  endWith();
  isCamelCase();
  isSnakeCase();
  isFalsy();
  isNaN();
  isFinite();
}
