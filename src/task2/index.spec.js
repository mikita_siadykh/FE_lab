/*
  eslint no-undef: 0
  no-unused-expressions: 0
*/
const expect = chai.expect;

describe('task 2', function() {
  describe('reverseString', function() {
    it('(abs) is sba', function() {
      expect(reverseString('abs')).to.equal('sba');
    });

    it('(123) is 321', function() {
      expect(reverseString('123')).to.equal('321');
    });
  });

  describe('startWith', function() {
    it('(hello world, hello) is true', function() {
      expect(startWith('hello world', 'hello')).to.be.true;
    });

    it('(hello world, "") is true', function() {
      expect(startWith('hello world', '')).to.be.true;
    });

    it('(hello, hello world) is false', function() {
      expect(startWith('hello', 'hello world')).to.be.false;
    });

    it('(hello world, world) is false', function() {
      expect(startWith('hello world', 'world')).to.be.false;
    });
  });

  describe('endWith', function() {
    it('(hello world, world) is true', function() {
      expect(endWith('hello world', 'world')).to.be.true;
    });

    it('(hello world, "") is true', function() {
      expect(endWith('hello world', '')).to.be.true;
    });

    it('(hello world, hello worlds) is false', function() {
      expect(endWith('hello', 'hello world')).to.be.false;
    });

    it('(hello world, land) is false', function() {
      expect(endWith('hello world', 'land')).to.be.false;
    });
  });

  describe('isCamelCase', function() {
    it('(iAmCamelCase) is true', function() {
      expect(isCamelCase('iAmCamelCase')).to.be.true;
    });

    it('(i_Am_CamelCase) is false', function() {
      expect(isCamelCase('i_Am_CamelCase')).to.be.false;
    });

    it('(i Am CamelCase) is false', function() {
      expect(isCamelCase('i Am CamelCase')).to.be.false;
    });

    it('(iAm.CamelCase) is false', function() {
      expect(isCamelCase('iAm.CamelCase')).to.be.false;
    });
  });

  describe('isSnakeCase', function() {
    it('(i_am_snake_case) is true', function() {
      expect(isSnakeCase('i_am_snake_case')).to.be.true;
    });

    it('(iamsnakecase) is false', function() {
      expect(isSnakeCase('iamsnakecase')).to.be.false;
    });

    it('(i Am SnakeCase) is false', function() {
      expect(isSnakeCase('i Am SnakeCase')).to.be.false;
    });

    it('(i_am_snakeCase) is false', function() {
      expect(isSnakeCase('i_am_snakeCase')).to.be.false;
    });
  });

  describe('isFalsy', function() {
    it('(false) is true', function() {
      expect(isFalsy(false)).to.be.true;
    });

    it('(null) is true', function() {
      expect(isFalsy(null)).to.be.true;
    });

    it('(0) is true', function() {
      expect(isFalsy(0)).to.be.true;
    });

    it('("") is true', function() {
      expect(isFalsy('')).to.be.true;
    });

    it('(NaN) is true', function() {
      expect(isFalsy(NaN)).to.be.true;
    });

    it('("0") is false', function() {
      expect(isFalsy('0')).to.be.false;
    });

    it('({}) is false', function() {
      expect(isFalsy({})).to.be.false;
    });
  });

  describe('isNaN', function() {
    it('(NaN) is true', function() {
      expect(isNaN(NaN)).to.be.true;
    });

    it('(2) is false', function() {
      expect(isNaN(2)).to.be.false;
    });
  });

  describe('isFinite', function() {
    it('(2) is true', function() {
      expect(isFinite(2)).to.be.true;
    });

    it('(2.5) is true', function() {
      expect(isFinite(2.5)).to.be.true;
    });

    it('(Infinite) is false', function() {
      expect(isFinite(Number.POSITIVE_INFINITY)).to.be.false;
    });

    it('(string) is false', function() {
      expect(isFinite('string')).to.be.false;
    });

    it('(string 22) is false', function() {
      expect(isFinite('22')).to.be.false;
    });
  });
});
