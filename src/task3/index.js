if (window.globals && !window.globals.isTest) {
  initial();
  compact();
  union();
  isArray();
  difference();
  uniq();
  rest();
  range();
}
