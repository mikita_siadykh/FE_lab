/*
  eslint no-undef: 0
  no-unused-expressions: 0
*/
const expect = chai.expect;

describe('task 3', function() {
  describe('initial', function() {
    const initialArray = [1, 2, 3, 4];

    it('([1, 2, 3, 4], 0) is [1, 2, 3, 4]', function() {
      expect(initial(initialArray, 0)).to.deep.equal([1, 2, 3, 4]);
    });

    it('([1, 2, 3, 4]) is [1, 2, 3]', function() {
      expect(initial(initialArray)).to.deep.equal([1, 2, 3]);
    });

    it('([1, 2, 3, 4], 1) is [1, 2, 3]', function() {
      expect(initial(initialArray, 1)).to.deep.equal([1, 2, 3]);
    });

    it('([1, 2, 3, 4], 2) is [1, 2]', function() {
      expect(initial(initialArray, 2)).to.deep.equal([1, 2]);
    });

    it('([1, 2, 3, 4], 6) is []', function() {
      expect(initial(initialArray, 6)).to.deep.equal([]);
    });

    it('([1, 2, 3, 4], -2) is [1, 2, 3, 4]', function() {
      expect(initial(initialArray, -2)).to.deep.equal([1, 2, 3, 4]);
    });
  });

  describe('compact', function() {
    const initialArray = [1, {}, [], false, '', null, 4, 0];

    it('([1, {}, [], false, "", null, 4, 0]) is [1, {}, [], 4]', function() {
      expect(compact(initialArray)).to.deep.equal([1, {}, [], 4]);
      expect(initialArray).to.deep.equal(initialArray);
    });
  });

  describe('union', function() {
    it('([1, 2], [2, 3], [3, 4], [{}, {}]) is [1, 2, 3, 4, {}, {}]', function() {
      expect(union([1, 2], [2, 3], [3, 4], [{}, {}])).to.deep.equal([1, 2, 3, 4, {}, {}]);
    });
  });

  describe('isArray', function() {
    it('("") is false', function() {
      expect(isArray('')).to.be.false;
    });

    it('({}) is false', function() {
      expect(isArray({})).to.be.false;
    });

    it('([1, 2, 3, 4]) is true', function() {
      expect(isArray([1, 2, 3, 4])).to.be.true;
    });

    it('(arguments) is false', function() {
      expect(isArray(arguments)).to.be.false;
    });
  });

  describe('difference', function() {
    const initialArray = [1, 2, 3, 4, 5];

    it('()', function() {
      expect(difference()).to.deep.equal([]);
    });

    it('([1, 2, 3, 4, 5])', function() {
      expect(difference(initialArray)).to.deep.equal([1, 2, 3, 4, 5]);
    });

    it('([1, 2, 3, 4, 5], [1, 2])', function() {
      expect(difference(initialArray, [1, 2])).to.deep.equal([3, 4, 5]);
    });

    it('([1, 2, 3, 4, 5], [1, 2], [5, 10])', function() {
      expect(difference(initialArray, [1, 2], [5, 10])).to.deep.equal([3, 4]);
    });
  });

  describe('uniq', function() {
    const initialArray = [1, 1, 2, 5, '', '', {}, { a: '123' }, {}];

    it('(1, 1, 2, 5, "", "", {}, { a: "123" }, {}) is [1, 2, 5, "", {}, { a: "123" }, {}', function() {
      expect(uniq(initialArray)).to.deep.equal([1, 2, 5, '', {}, { a: '123' }, {}]);
      expect(initialArray).to.deep.equal(initialArray);
    });
  });

  describe('rest', function() {
    const initialArray = [1, 2, 3, 4];

    it('([1, 2, 3, 4], 0) is [1, 2, 3, 4]', function() {
      expect(rest(initialArray, 0)).to.deep.equal([1, 2, 3, 4]);
    });

    it('([1, 2, 3, 4]) is [2, 3, 4]', function() {
      expect(rest(initialArray)).to.deep.equal([2, 3, 4]);
    });

    it('([1, 2, 3, 4], 1) is [2, 3, 4]', function() {
      expect(rest(initialArray, 1)).to.deep.equal([2, 3, 4]);
    });

    it('([1, 2, 3, 4], 2) is [3, 4]', function() {
      expect(rest(initialArray, 2)).to.deep.equal([3, 4]);
    });

    it('([1, 2, 3, 4], 6) is []', function() {
      expect(rest(initialArray, 6)).to.deep.equal([]);
    });

    it('([1, 2, 3, 4], -2) is [1, 2, 3, 4]', function() {
      expect(rest(initialArray, -2)).to.deep.equal([1, 2, 3, 4]);
    });
  });

  describe('range', function() {
    it('(10) is [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]', function() {
      expect(range(10)).to.deep.equal([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });

    it('(1, 10) is [1, 2, 3, 4, 5, 6, 7, 8, 9]', function() {
      expect(range(1, 10)).to.deep.equal([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });

    it('(1, 10, 3) is [1, 4, 7]', function() {
      expect(range(1, 10, 3)).to.deep.equal([1, 4, 7]);
    });

    it('(10, null, 3) is [0, 3, 6, 9]', function() {
      expect(range(10, null, 3)).to.deep.equal([0, 3, 6, 9]);
    });

    it('(10, null) is [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]', function() {
      expect(range(10, null)).to.deep.equal([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    });

    it('(-2, -5) is []', function() {
      expect(range(-2, -5)).to.deep.equal([]);
    });

    it('(-5, -2) is [-5, -4, -3]', function() {
      expect(range(-5, -2)).to.deep.equal([-5, -4, -3]);
    });

    it('(-5, -2, 2) is [-5, -3]', function() {
      expect(range(-5, -2, 2)).to.deep.equal([-5, -3]);
    });

    it('(-5, -2, -2) is []', function() {
      expect(range(-5, -2, -2)).to.deep.equal([]);
    });

    it('(-5, null, 2) is []', function() {
      expect(range(-5, null, 2)).to.deep.equal([]);
    });

    it('(-5) is []', function() {
      expect(range(-5)).to.deep.equal([]);
    });

    it('(-5, null, -2) is [0, -2, -4]', function() {
      expect(range(-5, null, -2)).to.deep.equal([0, -2, -4]);
    });

    it('(-10, -20, -5) is [-10, -15]', function() {
      expect(range(-10, -20, -5)).to.deep.equal([-10, -15]);
    });

    it('(-20, -10, -5) is []', function() {
      expect(range(-20, -10, -5)).to.deep.equal([]);
    });
  });
});
