/*
  eslint no-undef: 0
  no-unused-expressions: 0
*/
const expect = chai.expect;

describe('task 1', function() {
  describe('percentage', function() {
    it('(235, 5) is 10', function() {
      expect(percentage(235, 5)).to.equal(11.75);
    });

    it('(145.4, 18) is 26.17', function() {
      expect(percentage(145.4, 18)).to.equal(26.17);
    });

    it('(120, 20) is 24', function() {
      expect(percentage(120, 20)).to.equal(24);
    });

    it('(175, 22) is 38.5', function() {
      expect(percentage(175, 22)).to.equal(38.5);
    });
  });

  describe('factorial', function() {
    it('(-10) is NaN', function() {
      expect(factorial(-10)).to.be.NaN;
    });

    it('(0) is 1', function() {
      expect(factorial(0)).to.equal(1);
    });

    it('(5) is 120', function() {
      expect(factorial(5)).to.equal(120);
    });
  });

  describe('round', function() {
    it('(5.6) is 6', function() {
      expect(round(5.6)).to.equal(6);
    });

    it('(5.4) is 5', function() {
      expect(round(5.4)).to.equal(5);
    });

    it('(5.5) is 6', function() {
      expect(round(5.5)).to.equal(6);
    });

    it('(-5.6) is -6', function() {
      expect(round(-5.6)).to.equal(-6);
    });

    it('(-5.4) is -5', function() {
      expect(round(-5.4)).to.equal(-5);
    });

    it('(-5.5) is -5', function() {
      expect(round(-5.5)).to.equal(-5);
    });
  });

  describe('sqrN', function() {
    it('(2, 4.5) is NaN', function() {
      expect(sqrN(2, 4.5)).to.be.NaN;
    });

    it('(2, 0) is 1', function() {
      expect(sqrN(2, 0)).to.equal(1);
    });

    it('(1, 10) is 1', function() {
      expect(sqrN(1, 10)).to.equal(1);
    });

    it('(2, 3) is 8', function() {
      expect(sqrN(2, 3)).to.equal(8);
    });

    it('(2, -3) is 8', function() {
      expect(sqrN(2, -3)).to.equal(1 / 8);
    });
  });

  describe('abs', function() {
    it('(1) is 1', function() {
      expect(abs(1)).to.equal(1);
    });

    it('(-1) is 1', function() {
      expect(abs(-1)).to.equal(1);
    });
  });

  describe('sin', function() {
    it('(1) is 0.841', function() {
      expect(sin(1)).to.equal(0.841);
    });

    it('(0) is 0', function() {
      expect(sin(0)).to.equal(0);
    });

    it('(5) is -0.958', function() {
      expect(sin(5)).to.equal(-0.958);
    });
  });

  describe('cos', function() {
    it('(1) is 0.540', function() {
      expect(cos(1)).to.equal(0.54);
    });

    it('(0) is 1', function() {
      expect(cos(0)).to.equal(1);
    });

    it('(5) is 0.283', function() {
      expect(cos(5)).to.equal(0.283);
    });
  });

  describe('tg', function() {
    it('(1) is 1.557', function() {
      expect(tg(1)).to.equal(1.557);
    });

    it('(0) is 0', function() {
      expect(tg(0)).to.equal(0);
    });

    it('(5) is -3.380', function() {
      expect(tg(5)).to.equal(-3.38);
    });
  });
});
