// Karma configuration
// Generated on Sat Mar 17 2018 17:23:37 GMT+0300 (+03)

module.exports = function(config) {
  config.set({

    basePath: '',

    frameworks: ['mocha', 'chai'],

    exclude: [
    ],

    files: [
      './src/**/*.js',
      './src/**/*.spec.js'
    ],

    preprocessors: {
      './src/**/*.js': ['global']
    },

    reporters: ['progress'],

    port: 9876,

    colors: true,

    logLevel: config.LOG_INFO,

    autoWatch: false,

    browsers: ['PhantomJS'],

    singleRun: true,

    concurrency: Infinity,

    browserNoActivityTimeout: 100000,
    browserDisconnectTolerance: 2,

    failOnEmptyTestSuite: false,

    globals: {
      isTest: true,
    },
  })
}
